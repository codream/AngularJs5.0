import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgZorroAntdModule, NzNotificationService, NzMessageService} from 'ng-zorro-antd';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { CountdownModule } from 'ngx-countdown';

import { LayoutModule } from './layout/layout.module';
import { SharedModule } from './shared/shared.module';

import { JwtInterceptor } from './utils/jwt.interceptor';
import { DemandService } from './services/demand.service';
import { StorageService } from './services/storage.service';
import { AccountService } from './services/account.service';
import { HubService } from './services/hub.service';

@NgModule({
  declarations: [
    AppComponent
  ],

  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CountdownModule,
    BrowserAnimationsModule,
    SharedModule,
    LayoutModule,
    NgZorroAntdModule.forRoot()
  ],

  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    NzNotificationService,
    NzMessageService,
    StorageService,
    DemandService,
    AccountService,
    HubService
  ],

  bootstrap: [AppComponent]
})

export class AppModule {

}
