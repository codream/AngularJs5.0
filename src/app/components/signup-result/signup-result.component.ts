import { Component } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
    selector: 'app-signup-result',
    templateUrl: './signup-result.component.html',
    styleUrls: ['./signup-result.component.less']
})

export class SignupResultComponent {
    constructor(public msg: NzMessageService) { }
}
