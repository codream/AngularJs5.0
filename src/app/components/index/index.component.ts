import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { NzNotificationService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { HubService } from '../../services/hub.service';
import { NewsDetailsComponent } from '../news/news.component';
import { OrderDetailsComponent } from '../order/order.details.component';
import { StorageService } from '../../services/storage.service';
import { AccountService } from '../../services/account.service';
import { DemandService } from '../../services/demand.service';

import { News } from '../../classes/news';
import { Pack } from '../../classes/pack';
import { User } from '../../classes/user';
import { Demand } from '../../classes/demand';
import { Page } from '../../classes/page';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, OnDestroy {

  constructor(private hubService: HubService,
    private storage: StorageService,
    private router: Router,
    private accountService: AccountService,
    private demandService: DemandService,
    private notifyService: NzNotificationService) { }

  observer: Subscription;
  isVisible = false;
  isConfirmLoading = false;
  id: string;
  loading = true; // bug
  loadingMore = false;
  showClear = true;
  count = 0;
  news: Array<News> = new Array();
  user: User;
  observerUser: Subscription;
  selectedDemand: Demand;
  public page: Page<Demand>;
  public selected: Demand;
  observerPageDemand: Subscription;

  indeterminate = true;
  checkOptionsOne = [
    { label: '云狗专家', value: '云狗专家', checked: true },
    { label: '特殊工具', value: '特殊工具', checked: false },
    { label: 'VIN计算', value: 'VIN计算', checked: false }
  ];

  ngOnInit() {
    this.loading = false;
    this.observerPageDemand = this.demandService.page.subscribe({
      next: (data) => {
        this.page = data;
        this.count = this.page.totalElements;
      }
    });
    this.observer = this.hubService.subjectDemand.subscribe({
      next: (message: Pack) => {
        switch (message.action) {
          case 'CREATE':
            {
              const data = message.content;
              this.notifyService.info('新需求来了', data.vin);
              const ns: News = new News();
              ns.action = data.action;
              ns.target = message.target;
              ns.vin = data.vin;
              ns.carModelName = data.carModelName;
              ns.categoryName = data.categoryName;
              ns.disabled = false;
              this.news.push(ns);
              this.news = this.news.slice();
              break;
            }
          case 'DISABLE':
            {
              if (this.news) {
                for (const n of this.news) {
                  if (n.target === message.target) {
                    n.disabled = true;
                  }
                }
              }
              // $('#' + message.target).children('input').attr('disabled', true);
              break;
            }
          case 'ENABLE':
            {
              if (this.news) {
                for (const val of this.news) {
                  if (val.target === message.target) {
                    val.disabled = false;
                  }
                }
              }
              break;
            }
          case 'REMOVE':
            {
              // $('#' + message.target).remove();
              for (const n of this.news) {
                if (n.target === message.target) {
                  n.disabled = false;
                  this.news.forEach((val, idx) => {
                    if (val.target === message.target) {
                      this.news.splice(idx, 1);
                    }
                  });
                }
              }
              break;
            }
          default:
            {
              break;
            }
        }

      }
    });

    const JWT = this.storage.get('token');
    if (JWT) {
      this.observerUser = this.accountService.subjectUser.subscribe(
        {
          next: (data) => {
            this.user = data;
            this.hubService.connect(this.user.id);
          }
        }
      );
    } else {
      this.router.navigate(['/single/signin']);
    }
    this.accountService.getUser();
  }

  start() {
    this.demandService.loadPage();
  }

  onClear(): void {
    this.news = new Array();
  }

  onShowNews(item: News): void {
    this.id = item.target;
    // this.msg.success(item.email);
  }

  onRemove(item: any): void {
    const index = this.news.indexOf(item, 0);
    // this.news.slice(index, 1);
    this.news.splice(index, 1);
    // this.news =
    // this.msg.success(item.email);
  }

  showModal(item: Demand): void {
    this.isVisible = true;
    this.selectedDemand = item;
  }

  handleChange1(value: boolean): void {
    if (value) {
      this.hubService.join('云狗专家');
    } else {
      this.hubService.leave('云狗专家');
    }
  }

  handleChange2(value: boolean): void {
    if (value) {
      this.hubService.join('特殊工具');
    } else {
      this.hubService.leave('特殊工具');
    }
  }

  handleChange3(value: boolean): void {
    if (value) {
      this.hubService.join('VIN计算');
    } else {
      this.hubService.leave('VIN计算');
    }
  }

  ngOnDestroy() {
    if (this.observer) {
      this.observer.unsubscribe();
    }
    if (this.observerUser) {
      this.observerUser.unsubscribe();
    }
  }
}
