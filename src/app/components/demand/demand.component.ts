import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Demand } from '../../classes/demand';
import { Page } from '../../classes/page';

import { DemandService } from '../../services/demand.service';


@Component({
  selector: 'app-demand',
  templateUrl: './demand.component.html',
  styleUrls: ['./demand.component.css']
})

export class DemandComponent implements OnInit, OnDestroy {

  public page: Page<Demand>;
  public selected: Demand;
  observer: Subscription;
  // private observer: Observer<Page<Demand>>;
  constructor(private demandService: DemandService, private router: Router) {

  }

  ngOnInit() {
    this.observer = this.demandService.page.subscribe({
      next: (data) => this.page = data
    });
    this.demandService.loadPage();
  }

  onSelect(demand: Demand): void {
    this.selected = demand;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selected.id]);
  }

  ngOnDestroy() {
    this.observer.unsubscribe();
  }

  // add(name: string): void {
  //   name = name.trim();
  //   if (!name) { return; }
  //   this.heroService.create(name)
  //     .then(hero => {
  //       this.heroes.push(hero);
  //       this.selectedHero = null;
  //     });
  // }

  // delete(hero: Hero): void {
  //   this.heroService
  //       .delete(hero.id)
  //       .then(() => {
  //         this.heroes = this.heroes.filter(h => h !== hero);
  //         if (this.selectedHero === hero) { this.selectedHero = null; }
  //       });
  // }



}
