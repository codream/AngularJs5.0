import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { AccountService } from '../../services/account.service';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.less']
})
export class SignupComponent implements OnInit, OnDestroy {
  observer: Subscription;
  validateForm: FormGroup;
  error = '';
  type = 0;
  loading = false;
  visible = false;
  status = 'pool';
  progress = 0;
  passwordProgressMap = {
    ok: 'success',
    pass: 'normal',
    pool: 'exception'
  };

  count = 0;
  interval$: any;

  constructor(fb: FormBuilder, private router: Router, private msg: NzMessageService, private accountService: AccountService) {
    this.validateForm = fb.group({
      email: [null, [Validators.email]],
      idCard: [null, [Validators.required,
      Validators.pattern(/^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/)]],
      realName: [null, [Validators.required, Validators.pattern(/^[\u4e00-\u9fa5]{2,6}$/)]],
      password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(36), SignupComponent.checkPassword.bind(this)]],
      confirm: [null, [Validators.required, Validators.minLength(6), SignupComponent.passwordEquar]],
      mobilePrefix: ['+86'],
      cellphone: [null, [Validators.required, Validators.pattern(/^1\d{10}$/)]],
      captcha: [null, [Validators.required, Validators.maxLength(6)]]
    });
  }

  ngOnInit() {
    this.observer = this.accountService.subjectSignin.subscribe({
      next: (data) => {
        this.error = data;
        this.loading = false;
      }
    });
  }

  // tslint:disable-next-line:member-ordering
  static checkPassword(control: FormControl) {
    if (!control) { return null; }
    const self: any = this;
    self.visible = !!control.value;
    if (control.value && control.value.length > 9) {
      self.status = 'ok';
    } else if (control.value && control.value.length > 5) {
      self.status = 'pass';
    } else {
      self.status = 'pool';
    }
    if (self.visible) { self.progress = control.value.length * 10 > 100 ? 100 : control.value.length * 10; }
  }

  // tslint:disable-next-line:member-ordering
  static passwordEquar(control: FormControl) {
    if (!control || !control.parent) { return null; }
    if (control.value !== control.parent.get('password').value) {
      return { equar: true };
    }
    return null;
  }

  // region: fields

  get email() { return this.validateForm.controls.email; }
  get password() { return this.validateForm.controls.password; }
  get confirm() { return this.validateForm.controls.confirm; }
  get cellphone() { return this.validateForm.controls.cellphone; }
  get captcha() { return this.validateForm.controls.captcha; }
  get idCard() { return this.validateForm.controls.idCard; }
  get realName() { return this.validateForm.controls.realName; }

  // endregion

  // region: get captcha

  getCaptcha() {
    this.cellphone.markAsDirty();
    this.cellphone.updateValueAndValidity();
    if (this.cellphone.invalid) { return; }
    this.count = 59;
    this.interval$ = setInterval(() => {
      this.count -= 1;
      if (this.count <= 0) {
        clearInterval(this.interval$);
      }
    }, 1000);
    this.accountService.getSmsCaptcha(this.validateForm.value.cellphone);
  }

  // endregion

  submit() {
    this.error = '';
    // tslint:disable-next-line:forin
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    if (this.validateForm.invalid) { return; }
    // mock http
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      this.router.navigate(['/single/signin']);
    }, 1000);
  }

  ngOnDestroy(): void {
    this.observer.unsubscribe();
    if (this.interval$) { clearInterval(this.interval$); }
  }

}
