import { Component, OnInit, OnDestroy } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { Demand } from '../../classes/demand';
import { Page } from '../../classes/page';
import { HubService } from '../../services/hub.service';
import { DemandService } from '../../services/demand.service';

@Component({
  selector: 'app-news-details',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})

export class NewsDetailsComponent implements OnInit, OnDestroy {
  validateForm: FormGroup;
  // @Input() id: string;
  isVisible = false;
  isConfirmLoading = false;
  isDisabled = false;

  public demand: Demand;
  observer: Subscription;
  observerDemand: Subscription;
  observerAccepted: Subscription;
  message: string;
  constructor(private fb: FormBuilder,
    private demandService: DemandService,
    private hubService: HubService,
    private notifyService: NzNotificationService) { }

  ngOnInit() {
    this.isConfirmLoading = false;
    this.observer = this.demandService.single.subscribe({
      next: (data) => {
        this.demand = data;
        this.isVisible = true;
      }
    });
    this.observerAccepted = this.demandService.accepted.subscribe({
      next: (msg) => {
        this.isVisible = true;
        this.notifyService.info('提示', msg);
      },
      error: (msg) => {
        this.notifyService.error('提示', msg);
        this.isConfirmLoading = false;
      }
    });
    this.observer = this.hubService.subjectDemand.subscribe({
      next: (message) => {
        if (!this.demand || message.target !== this.demand.id) {
          return;
        }
        switch (message.action) {
          case 'ACCEPT':
            {
              if (message.result) {
                this.isVisible = false;
                this.isConfirmLoading = false;
                this.notifyService.success('提示', '抢单成功,等待客户付款.');
              } else {
                this.notifyService.error('提示', message.message);
                this.isVisible = false;
              }
              break;
            }
          case 'DISABLE':
            {
              this.isDisabled = true;
              this.notifyService.info('提示', '已有其它服务商抢单,等待客户确认中...');
              break;
            }
          case 'ENABLE':
            {
              this.isDisabled = false;
              this.notifyService.info('提示', '客户确选择了继续发布...');
              break;
            }
          case 'REMOVE':
            {
              this.isVisible = false;
              this.notifyService.info('提示', '已被其他服务商抢单');
              break;
            }
          default:
            {
              break;
            }
        }

      }
    });

    this.validateForm = this.fb.group({
      amount: [null, [Validators.required, Validators.min(5)]],
      minute: [null, [Validators.required, Validators.min(0)]],
      explain: [null],
    });
  }

  onSelect(demand: Demand): void {
    this.demand = demand;
  }

  gotoDetail(id: string): void {
    this.isConfirmLoading = false;
    this.demandService.loadSingle(id);
    // this.router.navigate(['/detail', this.selected.id]);
  }

  handleOk(): void {
    this.isConfirmLoading = true;
    // tslint:disable-next-line:forin
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    if (!this.validateForm.valid) {
      return;
    }

    this.demandService.accept(
      this.demand.id,
      this.validateForm.value.amount,
      this.validateForm.value.minute,
      this.validateForm.value.explain
    );
    // setTimeout(() => {
    //   this.isVisible = false;
    //   this.isConfirmLoading = false;
    // }, 3000);
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  submitForm(): void {
    // tslint:disable-next-line:forin
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  ngOnDestroy() {
    if (this.observer) {
      this.observer.unsubscribe();
    }
    if (this.observerDemand) {
      this.observerDemand.unsubscribe();
    }
    if (this.observerAccepted) {
      this.observerAccepted.unsubscribe();
    }
  }

}
