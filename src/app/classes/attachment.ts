export class Attachment {
  id: string;
  title: string;
  demandId: string;
  url: string;
  suffix: string;
  size: number;
  sort: number;
}
