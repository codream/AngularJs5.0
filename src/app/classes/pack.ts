export class Pack {
  type: string;
  action: string;
  target: string;
  message: string;
  result: boolean;
  content: any;
}
