export class Page<T> {
    public content: Array<T>;
    public first: boolean;
    public last: boolean;
    public totalPages: number;
    public totalElements: number;
    public numberOfElements: number;
    public size: number;
    public number: number;
}
