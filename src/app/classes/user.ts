import { UserInfo } from './user.info';
import { UserAsset } from './user.asset';
export class User {
  id: string;
  ukeyNumber: string;
  ukeyCode: string;
  cellphone: string;
  email: string;
  created: Date;
  status: number;
  userInfo: UserInfo;
  userAsset: UserAsset;
}
