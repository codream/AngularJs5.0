export class News {
  categoryName: string;
  target: string;
  action: string;
  vin: string;
  carModelName: string;
  created: Date;
  disabled: boolean;
}
