
import { Attachment } from './attachment';
export class Demand {
  id: string;
  title: string;
  amount: number;
  remarks: string;
  categoryName: string;
  code: string;

  tradingMode: number;
  created: number;
  publisherName: string;
  publisherAvatar: string;

  accepterStatus: number;
  acceptTime: number;

  payStatus: number;
  payTime: number;

  handleStatus: number;
  handleDoneTime: number;

  status: number;

  attachments: Array<Attachment>;
  resultAttachments: Array<Attachment>;
}
