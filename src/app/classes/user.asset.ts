export class UserAsset {
  integral: number;
  gold: number;
  food: number;
  cash: number;
  deposit: number;
  reward: number;
  annualFee: number;
}
