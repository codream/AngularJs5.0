import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// import { AppComponent } from './app.component';
import { LayoutDefaultComponent } from './layout/default/default.component';
import { LayoutSingleComponent } from './layout/single/single.component';

import { IndexComponent } from './components/index/index.component';
import { DemandComponent } from './components/demand/demand.component';
import { OrderComponent } from './components/order/order.component';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';

const appRoutes: Routes = [
  {
    path: '',
    component: LayoutDefaultComponent,
    children: [
      { path: '', redirectTo: '/index', pathMatch: 'full' },
      { path: 'index', component: IndexComponent },
      { path: 'demand', component: DemandComponent },
      { path: 'order', component: OrderComponent }
    ]
  },
  {
    path: 'single',
    component: LayoutSingleComponent,
    children: [
      { path: 'signin', component: SigninComponent },
      { path: 'signup', component: SignupComponent }
    ]
  }
  // 单页不包裹Layout
  // { path: 'lock', component: UserLockComponent, data: { title: '锁屏', titleI18n: 'lock' } },
  // { path: '403', component: Exception403Component },
  // { path: '404', component: Exception404Component },
  // { path: '500', component: Exception500Component },
  // { path: '**', redirectTo: 'dashboard' }
];
// { path: '**', component: PageNotFoundComponent }     { path: 'index', redirectTo: '/demand', pathMatch: 'full' },
@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false
      } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
