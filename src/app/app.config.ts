/**
 * 定义网站url根路由
 * @type {string}
 */
// export const ROOT_URL = 'http://192.168.2.110:8001/api/'; // 开发环境
export const ROOT_URL = '/api/'; // 代理环境
/**
 * 定义视频url根路由
 * @type {string}
 */
export const ROOT_VIDEO_URL = 'http://localhost:4200/'; // 本地开发

/**
 * 定义视频url根路由
 * @type {string}
 */
export const ROOT_UPLOAD_URL = 'http://localhost:4200/'; // 本地开发
