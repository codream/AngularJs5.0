import { Injectable, OnInit } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd';
import { Subject } from 'rxjs/Subject';
// import { Observable } from 'rxjs/Observable';
// import { Observer } from 'rxjs/Observer';
import { Pack } from '../classes/pack';
// import * as $ from 'jquery';
// import { SignalR } from 'angular-signalr-hub';
@Injectable()
export class HubService implements OnInit {

  public connection: any;
  public hub: any;
  public url: string;
  public uid: string;
  public push: any;
  public options: any;

  public subjectDemand: Subject<Pack> = new Subject<Pack>();

  constructor(private notifyService: NzNotificationService) {
    this.connection = $.connection;
    this.hub = this.connection.hub;
    this.push = this.connection.push;
    this.hub.url = 'http://192.168.2.98:8888/signalr';
    this.push.client.Message = (message: Pack) => {
      switch (message.type) {
        case 'DEMAND': {
          this.subjectDemand.next(message);
          break;
        }
        default:
          {
            this.notifyService.info('新的消息', message.message);
            break;
          }
      }
    };
  }

  ngOnInit() {


  }

  connect(uid: string) {

    this.hub.qs = {
      'uid': uid
    };

    this.hub.start()
      .done(() => {
        this.push.server.join('云狗专家');
        // this.notifyService.success('提示', '消息服务器连接成功!');
      })
      .fail(() => {
        this.notifyService.error('错误', '连接消息服务器失败,请重试..');
      });
  }

  join(group) {
    this.push.server.join(group);
  }

  leave(group) {
    this.push.server.leave(group);
  }
}
