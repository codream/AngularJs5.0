import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpErrorResponse } from '@angular/common/http';

// 4.x
// import {Observable} from 'rxjs';
// import 'rxjs/Rx';
// import { Headers, Http } from '@angular/http';
// import 'rxjs/add/operator/toPromise';

import { map, filter } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Demand } from '../classes/demand';
import { Page } from '../classes/page';
import { ROOT_URL } from '../app.config';

@Injectable()
export class DemandService {
  constructor(private http: HttpClient) { }

  public page: Subject<Page<Demand>> = new Subject<Page<Demand>>();
  public single: Subject<Demand> = new Subject<Demand>();
  public accepted: Subject<string> = new Subject<string>();

  public loadPage() {
    // Make the HTTP request: //<Page<Demand>> , { observe: 'response' | events } , { withCredentials: true }
    this.http.get<Page<Demand>>(ROOT_URL + 'demand/server/page').subscribe(res => {
      this.page.next(res);
    });
  }

  public loadSingle(id: string) {
    this.http.get<Demand>(ROOT_URL + 'demand/' + id).subscribe(res => {
      this.single.next(res);
    });
  }

  public accept(id: string, amount: number, minute: number, explain: string): void {
    // let data;
    this.http.post(ROOT_URL + 'demand/server/accept',
      {
        id: id,
        amount: amount,
        minute: minute,
        explain: explain
      }).subscribe(
        // Successful responses call the first callback.
        res => {
          this.accepted.next('已参与抢单,等待结果中...');
          // console.log(data);
        },
        // Errors will call this callback instead:
        err => {
          console.log(err);
          // throw new Error('抱歉,抢单未成功.');
          this.accepted.error('抱歉,抢单未成功.');
          this.accepted.complete();
        });
  }

  // getHero(id: number): Promise<Hero> {
  //   const url = `${this.heroesUrl}/${id}`;
  //   return this.http.get(url)
  //     .toPromise()
  //     .then(response => response.json().data as Hero)
  //     .catch(this.handleError);
  // }

  // delete(id: number): Promise<void> {
  //   const url = `${this.heroesUrl}/${id}`;
  //   return this.http.delete(url, {headers: this.headers})
  //     .toPromise()
  //     .then(() => null)
  //     .catch(this.handleError);
  // }

  // create(name: string): Promise<Hero> {
  //   return this.http
  //     .post(this.heroesUrl, JSON.stringify({name: name}), {headers: this.headers})
  //     .toPromise()
  //     .then(res => res.json().data as Hero)
  //     .catch(this.handleError);
  // }

  // update(hero: Hero): Promise<Hero> {
  //   const url = `${this.heroesUrl}/${hero.id}`;
  //   return this.http
  //     .put(url, JSON.stringify(hero), {headers: this.headers})
  //     .toPromise()
  //     .then(() => hero)
  //     .catch(this.handleError);
  // }
}
