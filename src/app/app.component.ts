import { Component, OnInit } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>'
})
// templateUrl: './app.component.html',
export class AppComponent implements OnInit {
  constructor(private notifyService: NzNotificationService) { }
  ngOnInit(): void {
    // this.notifyService.info('测试', '???');
  }
}
