import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { LayoutDefaultComponent } from './default/default.component';
import { LayoutFullScreenComponent } from './fullscreen/fullscreen.component';
import { LayoutSingleComponent } from './single/single.component';

import { NewsDetailsComponent } from '../components/news/news.component';
import { OrderDetailsComponent } from '../components/order/order.details.component';

import { NavComponent } from '../components/common/nav/nav.component';
import { SigninComponent } from '../components/signin/signin.component';
import { SignupComponent } from '../components/signup/signup.component';
import { AccountComponent } from '../components/account/account.component';
import { OrderComponent } from '../components/order/order.component';
import { DemandComponent } from '../components/demand/demand.component';
import { IndexComponent } from '../components/index/index.component';

const COMPONENTS = [
    LayoutDefaultComponent,
    LayoutSingleComponent,
    LayoutFullScreenComponent,
    SigninComponent,
    SignupComponent,
    AccountComponent,
    OrderComponent,
    DemandComponent,
    NewsDetailsComponent,
    OrderDetailsComponent,
    IndexComponent
];

const COMMON_COMPONENTS = [
    NavComponent
];

@NgModule({
    imports: [SharedModule],
    providers: [],
    declarations: [
        ...COMMON_COMPONENTS,
        ...COMPONENTS
    ],
    exports: [
        ...COMPONENTS
    ]
})

export class LayoutModule { }
