import { Component } from '@angular/core';
@Component({
  selector: 'app-layout-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.less']
})

export class LayoutSingleComponent {
  links = [
    {
      title: '帮助',
      href: ''
    },
    {
      title: '隐私',
      href: ''
    },
    {
      title: '条款',
      href: ''
    }
  ];
  constructor() { }
}
