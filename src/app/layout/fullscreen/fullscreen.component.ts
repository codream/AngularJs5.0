import { Component } from '@angular/core';

@Component({
    selector: 'app-layout-fullscreen',
    template: '<router-outlet></router-outlet>'
})

export class LayoutFullScreenComponent {
    constructor() { }
}
